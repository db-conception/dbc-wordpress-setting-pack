<?php

use \Owlana_Setting_Service;
use \Dbc_Setting_Admin;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * db-conception
 * 2021-02-04
 * 1.0
 * Provide functions to set/get customs settings of Owlana.fr
 */
class Owlana_Setting_Admin extends Dbc_Setting_Admin{

    /**
    * Constructeur de la classe
    *
    * @param void
    * @return void
    */
    public function __construct() {  
        $this->set_admin_menu_title("Owlana");
        $this->set_admin_page_slug("settings-owlana");
        $this->set_admin_page_title("Paramètre du site Owlana");
        // $this->set_admin_menu_icon();
        // $this->set_admin_menu_priority();
        $this->set_text_domaine("owlana");

        $this->init();
    }

    public function init(){
        parent::init();

        add_action( 'admin_post_TTB_settings_form_submit', [$this, 'TTB_settings_form_submit']);
    }
    public function admin_page_contents() {
        require_once("page-setting-owlana.php");
    }

    public function TTB_settings_form_submit(){
        if( isset( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], 'TTB_settings_form_submit') ) {
    
            // sanitize the input
            $slide1Content = wp_kses($_POST["editor-slide-1"], wp_kses_allowed_html( 'post' ));
            $slide2Content = wp_kses($_POST["editor-slide-2"], wp_kses_allowed_html( 'post' ));
            $slide3Content = wp_kses($_POST["editor-slide-3"], wp_kses_allowed_html( 'post' ));
            $TTB_bg = sanitize_hex_color($_POST["TTB_bg"]);
    
            // do the processing
            $service = Owlana_Setting_Service::getInstance();
            $service->setTTBSlide1Content( $slide1Content );
            $service->setTTBSlide2Content( $slide2Content );
            $service->setTTBSlide3Content( $slide3Content );
            $service->setTTBColor( $TTB_bg );

            // add the admin notice
            $admin_notice = "success";
    
            // redirect the user to the appropriate page
            wp_redirect( $_SERVER["HTTP_REFERER"], 302, 'WordPress' );
            exit;
            // $this->custom_redirect( $admin_notice, $_POST );
            // exit;
        }			
        else {
            wp_die( __( 'Invalid nonce specified', $this->get_admin_page_slug() ), 
                    __( 'Error', $this->get_admin_page_slug() ), array(
                        'response' 	=> 403,
                        'back_link' => 'admin.php?page=' . $this->get_admin_page_slug(),
    
                ) );
        }
    }

    public function register_admin_scripts() {
        wp_register_style( 'my-plugin', plugins_url( 'ddd/css/plugin.css' ) );
        wp_register_script( 'admin-owlana', get_template_directory_uri()."-child/admin-owlana.js", array( 'jquery', 'wp-color-picker' ), false, true);
    }
    
    
    public function load_scripts( $hook ) {
        // Load only on ?page=sample-page
        // var_dump( $hook );
        if( $hook != 'toplevel_page_'.$this->get_admin_page_slug() ) {
            return;
        }
    
        // Load style & scripts.
        wp_enqueue_style( 'wp-color-picker' );
        wp_enqueue_style( 'my-plugin' );
        wp_enqueue_script( 'admin-owlana' );
    }
   
}