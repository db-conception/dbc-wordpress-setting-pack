<?php

use \Dbc_Setting_Service;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if(!class_exists("Dbc_Setting_Admin"))
{

/**
 * db-conception
 * 2021-02-04
 * 1.0
 * Provide functions to set/get customs settings of Dbc.fr
 */
class Dbc_Setting_Admin {

    private $admin_page_title = 'Sample page';
    private $admin_page_slug = 'sample-page';
    private $admin_menu_title = 'Sample menu';
    private $admin_menu_icon = 'dashicons-schedule';
    private $admin_menu_priority = 100;
    private $text_domaine = 'my-textdomain';

    /**
     * Setter
     */
    public function set_admin_page_title($val){
        $this->admin_page_title = $val;
    }
    public function set_admin_page_slug($val){
        $this->admin_page_slug = $val;
    }
    public function set_admin_menu_title($val){
        $this->admin_menu_title = $val;
    }
    public function set_admin_menu_icon($val){
        $this->admin_menu_icon = $val;
    }
    public function set_admin_menu_priority($val){
        $this->admin_menu_priority = $val;
    }    
    public function set_text_domaine($val){
        $this->text_domaine = $val;
    }

    /**
     * Setter
     */
    public function get_admin_page_title(){
        return $this->admin_page_title;
    }
    public function get_admin_page_slug(){
        return $this->admin_page_slug;
    }
    public function get_admin_menu_title(){
        return $this->admin_menu_title;
    }
    public function get_admin_menu_icon(){
        return $this->admin_menu_icon;
    }
    public function get_admin_menu_priority(){
        return $this->admin_menu_priority;
    }    
    public function get_text_domaine(){
        return $this->text_domaine;
    }


    /**
    * Constructeur de la classe
    *
    * @param void
    * @return void
    */
    public function __construct() {  
        
    }
    
    public function init(){
        add_action( 'admin_menu', [$this, 'admin_menu'] );
        add_action( 'admin_enqueue_scripts', [$this, 'register_admin_scripts'] );
        add_action( 'admin_enqueue_scripts', [$this, 'load_scripts'] );
    }
    public function admin_menu() {
        add_menu_page(
            __( $this->admin_page_title, $this->text_domaine ),
            __( $this->admin_menu_title, $this->text_domaine ),
            'manage_options',
            $this->admin_page_slug,
            [$this, 'admin_page_contents'],
            $this->admin_menu_icon,
            $this->admin_menu_priority
        );
    }
    
    
    
    public function admin_page_contents() {
        ?>
        <h1>
            <?php esc_html_e( 'Welcome to my custom admin page.', 'my-plugin-textdomain' ); ?>
        </h1>
        <?php
    }
    
    
    
    public function register_admin_scripts() {
        wp_register_style( 'my-plugin', plugins_url( 'ddd/css/plugin.css' ) );
        wp_register_script( 'my-plugin', plugins_url( 'ddd/js/plugin.js' ) );
    }
    
    
    public function load_scripts( $hook ) {
        // Load only on ?page=sample-page
        // var_dump( $hook );
        if( $hook != 'toplevel_page_'.$this->get_admin_page_slug() ) {
            return;
        }
    
        // Load style & scripts.
        wp_enqueue_style( 'my-plugin' );
        wp_enqueue_script( 'my-plugin' );
    }
}
}