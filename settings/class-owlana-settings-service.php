<?php


use \Dbc_Settings_Service;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * db-conception
 * 2021-02-04
 * 1.0
 * Provide functions to set/get customs settings of Owlana.fr
 */
class Owlana_Setting_Service extends Dbc_Settings_Service{

    private static $_instance = null;

    /**
    * Constructeur de la classe
    *
    * @param void
    * @return void
    */
    private function __construct() {  
        add_filter("unero_get_option", [$this, "getTTBContentFilter"], 10, 2);
    }

    /*
    * Méthode qui crée l'unique instance de la classe
    * si elle n'existe pas encore puis la retourne.
    *
    * @param void
    * @return Singleton
    */
    public static function getInstance() {
 
        if(is_null(self::$_instance)) {
            self::$_instance = new Owlana_Setting_Service();  
        }

        return self::$_instance;
    }

    const TTB_SLIDE_1_CONTENT = "TTB_SLIDE_1_CONTENT";
    const TTB_SLIDE_2_CONTENT = "TTB_SLIDE_2_CONTENT";
    const TTB_SLIDE_3_CONTENT = "TTB_SLIDE_3_CONTENT";
    const TTB_BG = "TTB_BG";

    // hooks callback
    public function getTTBContentFilter($value, $name){
        if($name === "header_top_desc"){
            $filtered = do_shortcode($value);
            return $filtered;
        }
        return $value;
    }

    /**
     * Get the color in string hexa format
     * or false
     */
    public function getTTBColor(){
        $color = $this->getValue(TTB_BG);
        if(in_array($color, [null, false, '']))
            return false;
        return $color;
    }

    /**
     * Set the html content of the slide 1
     */
    public function setTTBColor($color){
        $this->setValue(TTB_BG, $color);
    }

    /**
     * Get the html content of the slide 1 
     * '' or value
     */
    public function getTTBSlide1Content(){
        return $this->getTTBSlideContent(TTB_SLIDE_1_CONTENT);
    }

    /**
     * Set the html content of the slide 1
     */
    public function setTTBSlide1Content($content){
        $this->setTTBSlideContent(TTB_SLIDE_1_CONTENT, $content);
    }

    /**
     * Get the html content of the slide 2
     * '' or value
     */
    public function getTTBSlide2Content(){
        return $this->getTTBSlideContent(TTB_SLIDE_2_CONTENT);
    }

    /**
     * Set the html content of the slide 2
     */
    public function setTTBSlide2Content($content){
        $this->setTTBSlideContent(TTB_SLIDE_2_CONTENT, $content);
    }

    /**
     * Get the html content of the slide 3 
     * '' or value
     */
    public function getTTBSlide3Content(){
        return $this->getTTBSlideContent(TTB_SLIDE_3_CONTENT);
    }

    /**
     * Set the html content of the slide 3
     */
    public function setTTBSlide3Content($content){
        $this->setTTBSlideContent(TTB_SLIDE_3_CONTENT, $content);
    }

    private function getTTBSlideContent($key){
        $content = $this->getValue($key);
        if(in_array($content, [null, false, '']))
            return "";
        return $content;
    }

    private function setTTBSlideContent($key, $content){
        $this->setValue($key, $content);
    }
    
}