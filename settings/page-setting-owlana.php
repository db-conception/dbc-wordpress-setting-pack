<?php
$settings_serv = Owlana_Setting_Service::getInstance();
$slide1Content = $settings_serv->getTTBSlide1Content();
$slide2Content = $settings_serv->getTTBSlide2Content();
$slide3Content = $settings_serv->getTTBSlide3Content();
$ttb_color = $settings_serv->getTTBColor();
?>

<h1>
    <?php esc_html_e( "Paramètre du site Owlana", 'my-plugin-textdomain' ); ?>
</h1>

<form method="POST" action="<?= admin_url( 'admin-post.php' ) ?>">
    <h4>Slide 1</h4>
    <?= wp_nonce_field("TTB_settings_form_submit") ?>
    <input type="hidden" name="action" value="TTB_settings_form_submit">
    
    <?= wp_editor( $slide1Content, "editor-slide-1") ?>
    <h4>Slide 2</h4>
    <?= wp_editor( $slide2Content, "editor-slide-2") ?>
    <h4>Slide 3</h4>
    <?= wp_editor( $slide3Content, "editor-slide-3") ?>
    <h3>Couleur</h3>
    <input type="text" name="TTB_bg" value="<?= (false !== $ttb_color ? $ttb_color : '') ?>" id="TTB_bg" data-default-color="#effeff" />
    <br>
    <input type="submit" value="Enregistrer">
</form>

